::

    $ NUM_REPLICAS=7; awk 'BEGIN { for (i = 0; i < '$NUM_REPLICAS'; i++) print "127.0.0.1"; }' > replicas.txt
    $ python3 scripts/gen_conf.py --ips replicas.txt --prefix 'hotstuff.gen' --keygen ./hotstuff-keygen --tls-keygen ./hotstuff-tls-keygen --pace-maker rr --block-size 400
    $ rm -f hotstuff.conf && ln -s hotstuff.gen.conf hotstuff.conf
